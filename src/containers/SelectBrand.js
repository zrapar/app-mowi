import React, { useEffect } from 'react';
import {
	Text,
	Dimensions,
	View,
	FlatList,
	BackHandler,
	TouchableOpacity,
	ScrollView,
	ActivityIndicator
} from 'react-native';
import { Image } from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { ScaledSheet } from 'react-native-size-matters';
import CustomHeader from '../components/CustomHeader';
import { useDispatch, useSelector } from 'react-redux';
import { getBrands } from '../redux/actions/brands';

const { height, width } = Dimensions.get('window');

const SelectBrand = ({ navigation }) => {
	const dispatch = useDispatch();
	const isConnected = useSelector(({ auth }) => auth.connectionStatus);
	const brands = useSelector(({ brands }) => brands.brands);
	const loadingBrands = useSelector(({ brands }) => brands.loadingBrands);

	// useEffect(() => {
	// 	if (isConnected) {
	// 		dispatch(getBrands());
	// 	}
	// }, []);

	const handleBackPress = () => {
		navigation.navigate('Home');
		return true;
	};

	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', handleBackPress);

		return () => {
			BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
		};
	}, []);

	const renderItem = ({ item, ...rest }) => {
		return (
			<View style={style.viewButton}>
				<TouchableOpacity
					onPress={() => {
						navigation.navigate({
							routeName : 'SelectType',
							params    : item.uuid
						});
					}}
					style={style.containerBtn}
				>
					<View>
						<Image
							source={{ uri: item.image, cache: 'only-if-cached' }}
							style={{ width: scale(150), height: verticalScale(150) }}
							PlaceholderContent={<ActivityIndicator />}
						/>
					</View>
				</TouchableOpacity>
				<Text style={style.title}>{item.title}</Text>
			</View>
		);
	};

	return (
		<React.Fragment>
			<CustomHeader title='BRANDS' />
			{loadingBrands ? (
				<View style={{ flex: 1, justifyContent: 'center' }}>
					<ActivityIndicator size='large' color='#f3704e' />
				</View>
			) : (
				<View style={style.viewContainer}>
					<ScrollView style={{ marginBottom: scale(20) }}>
						<FlatList
							contentContainerStyle={style.containerList}
							scrollEventThrottle={1000}
							data={brands}
							numColumns={3}
							renderItem={renderItem}
							keyExtractor={(item) => item.uuid}
						/>
					</ScrollView>
				</View>
			)}
		</React.Fragment>
	);
};

const style = ScaledSheet.create({
	viewContainer : {
		width  : `${width}@s`,
		height : `${height}@vs`,
		margin : '0@vs'
	},
	containerList : {
		justifyContent : 'center',
		flexDirection  : 'column',
		flex           : 1
	},
	viewButton    : {
		margin         : '8@s',
		justifyContent : 'center',
		alignItems     : 'center'
	},
	containerBtn  : {
		justifyContent  : 'center',
		alignItems      : 'center',
		backgroundColor : '#fff',
		width           : '150@s',
		height          : '150@vs',
		overflow        : 'hidden',
		position        : 'relative',
		borderWidth     : 1,
		borderRadius    : 15,
		borderColor     : 'transparent',
		shadowColor     : '#fa4169',
		shadowOffset    : { width: 0, height: 2 },
		shadowOpacity   : 0.5,
		shadowRadius    : 1,
		elevation       : 8,
		marginLeft      : '10@vs',
		marginRight     : '10@vs'
	},
	title         : {
		color      : '#73628B',
		fontWeight : 'bold',
		textAlign  : 'left',
		fontSize   : '15@vs'
	}
});

export default SelectBrand;
