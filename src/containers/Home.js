import React, { useEffect } from 'react';
import { ImageBackground, Text, Dimensions, View, BackHandler, Alert } from 'react-native';
import { Button } from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { ScaledSheet } from 'react-native-size-matters';
import CustomHeader from '../components/CustomHeader';
import imgBg from '../assets/images/mowi-fondo.jpg';
const { height, width } = Dimensions.get('window');

const Home = ({ navigation }) => {
	const handleBackPress = () => {
		Alert.alert(
			'Exit App',
			'Do you want to exit?',
			[
				{ text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{ text: 'Yes', onPress: () => BackHandler.exitApp() }
			],
			{ cancelable: false }
		);
		return true;
	};

	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', handleBackPress);

		return () => {
			BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
		};
	}, []);

	return (
		<React.Fragment>
			<CustomHeader />
			<ImageBackground
				source={imgBg}
				resizeMode='cover'
				style={{
					width          : '100%',
					height         : '100%',
					justifyContent : 'flex-end'
				}}
			>
				<Text
					style={{
						alignSelf  : 'flex-start',
						marginLeft : scale(30),
						fontSize   : scale(60),
						color      : '#fff',
						fontFamily : 'GothamBold'
					}}
				>
					Marketing
				</Text>
				<Text
					style={{
						alignSelf  : 'flex-start',
						marginLeft : scale(30),
						fontSize   : scale(60),
						color      : '#fff',
						fontFamily : 'GothamBold'
					}}
				>
					Tooltips
				</Text>
				<Button
					onPress={() => navigation.navigate('SelectBrand')}
					containerStyle={{
						alignSelf     : 'flex-start',
						marginLeft    : scale(30),
						marginTop     : scale(20),
						paddingBottom : scale(90)
					}}
					buttonStyle={{
						backgroundColor : '#f3704e'
					}}
					titleStyle={{
						fontFamily : 'GothamBold'
					}}
					title='Let&#39;s get started'
				/>
			</ImageBackground>
		</React.Fragment>
	);
};

export default Home;
