import React, { useEffect } from 'react';
import { ImageBackground, Text, Dimensions, View, Alert, BackHandler } from 'react-native';
import { Button } from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { ScaledSheet } from 'react-native-size-matters';
import CustomHeader from '../components/CustomHeader';
import imgBg from '../assets/images/mowi-fondo.jpg';
const { height, width } = Dimensions.get('window');

const Home = ({ navigation }) => {
	const handleBackPress = () => {
		Alert.alert(
			'Exit App',
			'Do you want to exit?',
			[
				{ text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
				{ text: 'Yes', onPress: () => BackHandler.exitApp() }
			],
			{ cancelable: false }
		);
		return true;
	};

	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', handleBackPress);

		return () => {
			BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
		};
	}, []);
	return (
		<React.Fragment>
			<CustomHeader />
			<ImageBackground
				source={imgBg}
				resizeMode='cover'
				style={{
					width          : '100%',
					height         : '100%',
					justifyContent : 'center'
				}}
			>
				<Text
					style={{
						marginTop  : scale(-50),
						marginLeft : scale(30),
						fontSize   : scale(45),
						color      : '#fff',
						fontWeight : 'bold',
						fontFamily : 'GothamBold'
					}}
				>
					You need to be connected to get the data app
				</Text>
				<Button
					onPress={() => navigation.navigate('AuthLoading')}
					containerStyle={{
						alignSelf     : 'flex-start',
						marginLeft    : scale(30),
						marginTop     : scale(20),
						paddingBottom : scale(50)
					}}
					buttonStyle={{
						backgroundColor : '#f3704e',
						fontFamily      : 'GothamBold'
					}}
					title='Try Again'
				/>
			</ImageBackground>
		</React.Fragment>
	);
};

export default Home;
