import React, { useState, useEffect } from 'react';
import {
	Text,
	Dimensions,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	ActivityIndicator,
	BackHandler,
	ImageBackground,
	SafeAreaView,
	Image as ImgRN
} from 'react-native';
import { Image, Button } from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { ScaledSheet } from 'react-native-size-matters';
import CustomHeader from '../components/CustomHeader';
import { useDispatch, useSelector } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Accordion from 'react-native-collapsible/Accordion';

const { height, width } = Dimensions.get('window');
const SECTIONS = [
	{
		title   : 'Brand Presentation',
		content : 'Lorem ipsum...'
	},
	{
		title   : 'Products',
		content : 'Lorem ipsum...'
	},
	{
		title   : 'Marketing',
		content : 'Lorem ipsum...'
	},
	{
		title   : 'Line List',
		content : 'Lorem ipsum...'
	},
	{
		title   : 'Video Content',
		content : 'Lorem ipsum...'
	}
];
const Consumer = ({ navigation }) => {
	const Brand = navigation.state.params;
	// console.log(navigation);
	const [ activeSections, setActiveSection ] = useState([]);

	const handleBackPress = () => {
		navigation.navigate({
			routeName : 'SelectType',
			params    : Brand.uuid
		});
		return true;
	};

	const _renderHeader = (section) => {
		return (
			<View
				style={{
					backgroundColor   : '#58595b',
					borderBottomColor : '#222',
					paddingVertical   : scale(6),
					paddingLeft       : scale(20),
					marginVertical    : -1,
					shadowColor       : '#000',
					shadowOffset      : { width: 1, height: 2 },
					shadowOpacity     : 1,
					shadowRadius      : 2,
					elevation         : 10
				}}
			>
				<Text style={{ color: '#fff', fontSize: scale(18) }}>{section.title}</Text>
			</View>
		);
	};

	const _renderContent = (section) => {
		return (
			<View>
				<Text>{section.content}</Text>
			</View>
		);
	};

	const _updateSections = (activeSections) => {
		setActiveSection(activeSections);
	};

	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', handleBackPress);

		return () => {
			BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
		};
	}, []);

	return (
		<React.Fragment>
			<CustomHeader title='BRANDS' subtitle={Brand.name} color={Brand.color} />
			<Grid>
				<Row>
					<ImageBackground
						source={{ uri: Brand.background, cache: 'only-if-cached' }}
						style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}
					>
						<Image
							source={{ uri: Brand.image, cache: 'only-if-cached' }}
							resizeMode='contain'
							style={{ width: scale(200), height: '90%' }}
							PlaceholderContent={<ActivityIndicator size='large' color={Brand.color} />}
						/>
					</ImageBackground>
				</Row>
				<Row style={{ backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', marginTop: -1 }}>
					<SafeAreaView>
						<ScrollView>
							<Accordion
								style={{ width: scale(width - 17) }}
								sections={SECTIONS}
								activeSections={activeSections}
								renderHeader={_renderHeader}
								renderContent={_renderContent}
								onChange={_updateSections}
							/>
						</ScrollView>
					</SafeAreaView>
				</Row>
			</Grid>
		</React.Fragment>
	);
};

const style = ScaledSheet.create({
	viewContainer : {
		width  : `${width}@s`,
		height : `${height}@vs`,
		margin : '0@vs'
	},
	containerList : {
		justifyContent : 'center',
		flexDirection  : 'column',
		flex           : 1
	},
	viewButton    : {
		margin         : '8@s',
		justifyContent : 'center',
		alignItems     : 'center'
	},
	containerBtn  : {
		justifyContent  : 'center',
		alignItems      : 'center',
		backgroundColor : '#fff',
		width           : '150@s',
		height          : '150@vs',
		overflow        : 'hidden',
		position        : 'relative',
		borderWidth     : 1,
		borderRadius    : 15,
		borderColor     : 'transparent',
		shadowColor     : '#fa4169',
		shadowOffset    : { width: 0, height: 2 },
		shadowOpacity   : 0.5,
		shadowRadius    : 1,
		elevation       : 8,
		marginLeft      : '10@vs',
		marginRight     : '10@vs'
	},
	title         : {
		color      : '#73628B',
		fontWeight : 'bold',
		textAlign  : 'left',
		fontSize   : '15@vs'
	}
});

export default Consumer;
