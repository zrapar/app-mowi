import React, { useState, useEffect } from 'react';
import {
	Text,
	Dimensions,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	ActivityIndicator,
	BackHandler,
	ImageBackground,
	Image as ImgRN
} from 'react-native';
import { Image, Button } from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { ScaledSheet } from 'react-native-size-matters';
import CustomHeader from '../components/CustomHeader';
import { useDispatch, useSelector } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import AccordionInner from '../components/AccordionInner';
import { AccordionList } from 'accordion-collapse-react-native';

const { height, width } = Dimensions.get('window');

const Features = ({ navigation }) => {
	let Type = navigation.state.params;
	if (typeof Type === 'object') {
		Type = Object.keys(navigation.state.params)
			.map((key, index) => {
				return navigation.state.params[index];
			})
			.join('');
	}

	const Brand = useSelector(({ brands }) => brands.selectedBrand);

	const [ state, setState ] = useState({
		sections      : [
			{
				title   : 'Brand Presentation',
				content : 'presentations',
				show    : true
			},
			{
				title   : 'Products',
				content : 'products',
				show    : true
			},
			{
				title   : 'Marketing',
				content : 'marketings',
				show    : true
			},
			{
				title   : 'Line List',
				content : 'linelists',
				show    : true
			},
			{
				title   : 'Video Content',
				content : 'videos',
				show    : true
			}
		],
		activeSection : false
	});

	const { sections, activeSection } = state;

	const handleBackPress = () => {
		navigation.navigate({
			routeName : 'SelectType',
			params    : Brand.uuid.toString()
		});
		return true;
	};

	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', handleBackPress);

		return () => {
			BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
		};
	}, []);

	const _head = (item) => {
		return (
			item.show && (
				<View style={[ styles.header, typeof activeSection === 'number' && styles.headerFixed ]}>
					<Text style={styles.headerText}>{item.title}</Text>
				</View>
			)
		);
	};

	const toggle = (item) => {
		if (typeof activeSection === 'number') {
			const newSections = sections.map((element) => {
				return {
					...element,
					show : true
				};
			});

			setState({ sections: newSections, activeSection: false });
		}

		if (typeof activeSection === 'boolean') {
			const newSections = sections.map((element, index) => {
				if (index !== item) {
					return {
						...element,
						show : false
					};
				}
				return element;
			});

			setState({ sections: newSections, activeSection: item });
		}
	};

	const _body = (item) => {
		return <AccordionInner feature={item.content} type={Type} navigation={navigation} />;
	};

	return (
		<React.Fragment>
			<CustomHeader title='BRANDS' subtitle={Brand.name} color={Brand.color} />
			<Grid>
				<Row>
					<ImageBackground source={{ uri: Brand.background }} style={styles.imageBg} />
				</Row>
				<Row style={[ styles.rowView, typeof activeSection === 'number' && styles.containerFixed ]}>
					<AccordionList list={state.sections} header={_head} body={_body} onToggle={toggle} />
				</Row>
			</Grid>
		</React.Fragment>
	);
};

const styles = ScaledSheet.create({
	imageBg        : { width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' },
	rowView        : { backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' },
	viewContainer  : {
		width           : `${width - 18}@s`,
		// height          : `30@s`,
		// margin          : '0@vs',
		backgroundColor : '#58595b'
	},
	containerList  : {
		justifyContent : 'center',
		flexDirection  : 'column',
		flex           : 1
	},
	viewButton     : {
		margin         : '8@s',
		justifyContent : 'center',
		alignItems     : 'center'
	},
	containerBtn   : {
		justifyContent  : 'center',
		alignItems      : 'center',
		backgroundColor : '#fff',
		width           : '150@s',
		height          : '150@vs',
		overflow        : 'hidden',
		position        : 'relative',
		borderWidth     : 1,
		borderRadius    : 15,
		borderColor     : 'transparent',
		shadowColor     : '#fa4169',
		shadowOffset    : { width: 0, height: 2 },
		shadowOpacity   : 0.5,
		shadowRadius    : 1,
		elevation       : 8,
		marginLeft      : '10@vs',
		marginRight     : '10@vs'
	},
	title          : {
		color      : '#73628B',
		textAlign  : 'left',
		fontSize   : '15@vs',
		fontFamily : 'GothamBold'
	},
	header         : {
		paddingVertical : '8@vs',
		paddingLeft     : '20@s',
		alignItems      : 'flex-start',
		backgroundColor : '#58595b',
		textAlign       : 'left',
		elevation       : 20
	},
	containerFixed : {
		alignItems : 'flex-start'
	},
	headerFixed    : {
		alignItems : 'center'
	},
	headerText     : {
		fontFamily : 'GothamMedium',
		color      : '#fff',
		textAlign  : 'left'
	}
});

export default Features;
