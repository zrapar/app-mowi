import React, { useState, useEffect } from 'react';
import {
	Text,
	Dimensions,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	ActivityIndicator,
	BackHandler,
	ImageBackground,
	Image as ImgRN
} from 'react-native';
import { Image, Button } from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { ScaledSheet } from 'react-native-size-matters';
import CustomHeader from '../components/CustomHeader';
import { useDispatch, useSelector } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { getBrand, cleanSelectedBrand } from '../redux/actions/brands';

const { height, width } = Dimensions.get('window');

const SelectType = ({ navigation }) => {
	let uuid = navigation.state.params;
	if (typeof uuid === 'object') {
		uuid = Object.keys(navigation.state.params)
			.map((key, index) => {
				return navigation.state.params[index];
			})
			.join('');
	}

	const dispatch = useDispatch();
	// const brands = useSelector(({ brands }) => brands.brands);
	const brand = useSelector(({ brands }) => brands.selectedBrand);
	// const brandObtained = useSelector(({ brands }) => brands.getBrand);

	useEffect(
		() => {
			if (typeof uuid === 'string') {
				dispatch(getBrand(uuid));
			}
		},
		[ uuid ]
	);

	const handleBackPress = () => {
		navigation.navigate('SelectBrand');
		dispatch(cleanSelectedBrand());
		return true;
	};

	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', handleBackPress);

		return () => {
			BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
		};
	}, []);

	const goToType = (view) => {
		navigation.navigate({
			routeName : 'Features',
			params    : view
		});
		return true;
	};

	return (
		<React.Fragment>
			{brand ? (
				<React.Fragment>
					<CustomHeader title='BRANDS' subtitle={brand.name} color={brand.color} />
					<Grid>
						<Row>
							<ImageBackground
								source={{ uri: brand.background, cache: 'only-if-cached' }}
								style={{
									width          : '100%',
									height         : '100%',
									justifyContent : 'center',
									alignItems     : 'center'
								}}
							>
								{/* <Image
									source={{ uri: brand.image, cache: 'only-if-cached' }}
									resizeMode='contain'
									style={{ width: scale(200), height: '90%' }}
									PlaceholderContent={<ActivityIndicator size='large' color={brand.color} />}
								/> */}
							</ImageBackground>
						</Row>
						<Row style={{ backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
							<Col>
								<Button
									onPress={() => goToType('consumer')}
									buttonStyle={{ backgroundColor: brand.color }}
									containerStyle={{ width: '90%', marginLeft: '5%' }}
									titleStyle={{ fontFamily: 'GothamMedium' }}
									title={`Consumer Package Goods`}
								/>
							</Col>
							<Col>
								<Button
									onPress={() => goToType('foods')}
									buttonStyle={{ backgroundColor: brand.color }}
									containerStyle={{ width: '90%', marginLeft: '5%' }}
									titleStyle={{ fontFamily: 'GothamMedium' }}
									title='Food Service'
								/>
							</Col>
						</Row>
					</Grid>
				</React.Fragment>
			) : (
				<View style={{ flex: 1, justifyContent: 'center' }}>
					<ActivityIndicator size='large' color='#f3704e' />
				</View>
			)}
		</React.Fragment>
	);
};

const style = ScaledSheet.create({
	viewContainer : {
		width  : `${width}@s`,
		height : `${height}@vs`,
		margin : '0@vs'
	},
	containerList : {
		justifyContent : 'center',
		flexDirection  : 'column',
		flex           : 1
	},
	viewButton    : {
		margin         : '8@s',
		justifyContent : 'center',
		alignItems     : 'center'
	},
	containerBtn  : {
		justifyContent  : 'center',
		alignItems      : 'center',
		backgroundColor : '#fff',
		width           : '150@s',
		height          : '150@vs',
		overflow        : 'hidden',
		position        : 'relative',
		borderWidth     : 1,
		borderRadius    : 15,
		borderColor     : 'transparent',
		shadowColor     : '#fa4169',
		shadowOffset    : { width: 0, height: 2 },
		shadowOpacity   : 0.5,
		shadowRadius    : 1,
		elevation       : 8,
		marginLeft      : '10@vs',
		marginRight     : '10@vs'
	},
	title         : {
		color      : '#73628B',
		fontWeight : 'bold',
		textAlign  : 'left',
		fontSize   : '15@vs'
	}
});

export default SelectType;
