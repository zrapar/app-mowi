import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Text, View, ActivityIndicator, Alert } from 'react-native';
import { login, checkUser, connectionStatus } from '../redux/actions/auth';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import { getBrands } from '../redux/actions/brands';
import RNFetchBlob from 'rn-fetch-blob';

const AuthLoading = ({ navigation }) => {
	let firstTime;
	const dispatch = useDispatch();
	const user = useSelector(({ auth }) => auth.user);
	const isAuth = useSelector(({ auth }) => auth.isAuth);
	const token = useSelector(({ auth }) => auth.token);
	const loadingAuth = useSelector(({ auth }) => auth.loadingAuth);
	const error = useSelector(({ auth }) => auth.error);
	const isConnected = useSelector(({ auth }) => auth.connectionStatus);
	const brands = useSelector(({ brands }) => brands.brands);

	const _fetchData = async () => {
		try {
			firstTime = await AsyncStorage.getItem('@firstTime');
		} catch (error) {}
	};

	useEffect(
		() => {
			_fetchData();
			NetInfo.addEventListener(({ isConnected }) => {
				dispatch(connectionStatus(isConnected));
			});
		},
		[ dispatch, isConnected ]
	);

	useEffect(
		() => {
			_fetchData();
			if (!user) {
				dispatch(login(navigation));
			}

			if (user && isConnected && brands.length > 0) {
				Alert.alert(
					'Update data',
					'You want update the current data',
					[
						{ text: 'No', onPress: () => navigation.navigate('SelectBrand') },
						{
							text    : 'Yes',
							onPress : () => {
								RNFetchBlob.session('documents').dispose().then(() => {
									RNFetchBlob.session('brands').dispose().then(() => {
										RNFetchBlob.session('videos').dispose().then(() => {
											dispatch(getBrands(navigation));
										});
									});
								});
							}
						}
					],
					{ cancelable: false }
				);
			}

			if (user && isConnected && brands.length === 0) {
				dispatch(getBrands(navigation));
			}
			if (user && !isConnected && brands.length > 0) {
				navigation.navigate('Home');
			}
		},
		[ user ]
	);

	return (
		<View style={{ flex: 1, justifyContent: 'center' }}>
			<ActivityIndicator size='large' color='#f3704e' />
		</View>
	);
};

export default AuthLoading;
