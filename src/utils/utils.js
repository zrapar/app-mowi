import RNFetchBlob from 'rn-fetch-blob';

const Fetch = RNFetchBlob.config({
	fileCache : true
});

export const convertUrlToCacheFile = async (url, feature) => {
	return new Promise((resolve) => {
		Fetch.fetch('GET', url).then((response) => {
			response.session(feature);
			resolve('file://' + response.path());
		});
	});
};

export const convertToPromise = async (brands) => {
	const data = brands.map((element) => {
		return {
			...element,
			image      : convertUrlToCacheFile(element.image, 'brands'),
			background : convertUrlToCacheFile(element.background, 'brands')
		};
	});
	return data;
};

export const resolvePromises = async (brands) => {
	let arrayFinal = [];
	brands.then((arrayBrands) => {
		console.log('arrayBrands', arrayBrands);
		arrayBrands.forEach((element, index) => {
			console.log('element', element);
			element.image.then((imageResponse) => {
				element.background.then((backgroundResponse) => {
					console.log('imageResponse', imageResponse);
					console.log('backgroundResponse', backgroundResponse);
				});
			});
		});
	});
	console.log('arrayFinal', arrayFinal);

	// brandsArray.reduce((p, x) => p.then((_) => myPromise(x)), Promise.resolve());
};
