import axios from 'axios';

const axiosInstance = axios.create({
	baseURL : 'https://api.captainomega.com/api'
	/* other custom settings */
});

axiosInstance.interceptors.request.use((request) => {
	console.log('Starting Request:', request);
	return request;
});

axiosInstance.interceptors.response.use((response) => {
	console.log('Response:', response);
	return response;
});

module.exports = axiosInstance;
