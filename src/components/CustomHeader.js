import React, { useEffect } from 'react';
import { Header } from 'react-native-elements';
import { ScaledSheet } from 'react-native-size-matters';

const CustomHeader = ({ title, subtitle, color }) => {
	return (
		<Header
			placement='left'
			containerStyle={[ styles.container, { backgroundColor: color ? color : '#f3704e' } ]}
			leftComponent={{
				text  : 'MOWI USA',
				style : styles.leftLetters
			}}
			rightComponent={
				title ? (
					{
						text  : subtitle ? `/ ${title} / ${subtitle}` : `/ ${title}`,
						style : styles.rightLetters
					}
				) : null
			}
		/>
	);
};

const styles = ScaledSheet.create({
	container    : {
		backgroundColor : '#f3704e',
		display         : 'flex',
		alignItems      : 'center',
		justifyContent  : 'center',
		height          : '40@vs',
		marginBottom    : -1
	},
	leftLetters  : {
		color      : '#fff',
		marginTop  : '-20@ms',
		fontSize   : '18@ms',
		fontFamily : 'GothamBook'
	},
	rightLetters : {
		color      : '#fff',
		marginTop  : '-20@ms',
		fontSize   : '18@ms',
		fontFamily : 'GothamBook'
	}
});

export default CustomHeader;
