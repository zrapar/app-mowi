import React, { useEffect } from 'react';
import { StyleSheet, Dimensions, View, BackHandler } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { ScaledSheet } from 'react-native-size-matters';

import Pdf from 'react-native-pdf';

const PDFView = ({ navigation }) => {
	const params = navigation.state.params;
	const handleBackPress = () => {
		navigation.navigate({
			routeName : 'Features',
			params    : params.view.toString()
		});
		return true;
	};

	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', handleBackPress);

		return () => {
			BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
		};
	}, []);

	const source = { uri: params.file };

	return (
		<View style={styles.container}>
			<Pdf
				source={source}
				onLoadComplete={(numberOfPages, filePath) => {
					console.log(`number of pages: ${numberOfPages}`);
				}}
				onPageChanged={(page, numberOfPages) => {
					console.log(`current page: ${page}`);
				}}
				onError={(error) => {
					console.log(error);
				}}
				onPressLink={(uri) => {
					console.log(`Link presse: ${uri}`);
				}}
				style={styles.pdf}
			/>
		</View>
	);
};

export default PDFView;

const styles = ScaledSheet.create({
	container : {
		flex           : 1,
		justifyContent : 'flex-start',
		alignItems     : 'center',
		marginTop      : '25@s'
	},
	pdf       : {
		flex   : 1,
		width  : scale(Dimensions.get('window').width),
		height : scale(Dimensions.get('window').height)
	}
});
