import React, { useEffect, useState } from 'react';
import { StyleSheet, Dimensions, View, BackHandler, TouchableOpacity, Text } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { ScaledSheet } from 'react-native-size-matters';

import Video from 'react-native-video';

const VideoView = ({ navigation }) => {
	const params = navigation.state.params;
	const handleBackPress = () => {
		navigation.navigate({
			routeName : 'Features',
			params    : params.view.toString()
		});
		return true;
	};

	let video;

	const [ state, setState ] = useState({
		rate        : 1,
		volume      : 1,
		muted       : false,
		resizeMode  : 'contain',
		duration    : 0.0,
		currentTime : 0.0,
		paused      : true
	});

	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', handleBackPress);

		return () => {
			BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
		};
	}, []);

	const source = { uri: params.file };

	const onLoad = (data) => {
		setState({ ...state, duration: data.duration });
	};

	const onProgress = (data) => {
		setState({ ...state, currentTime: data.currentTime });
	};

	const onEnd = () => {
		setState({ ...state, paused: true });
		video.seek(0);
	};

	const onAudioBecomingNoisy = () => {
		setState({ ...state, paused: true });
	};

	const onAudioFocusChanged = (event) => {
		setState({ ...state, paused: !event.hasAudioFocus });
	};

	const getCurrentTimePercentage = () => {
		if (state.currentTime > 0) {
			return parseFloat(state.currentTime) / parseFloat(state.duration);
		}
		return 0;
	};

	const flexCompleted = getCurrentTimePercentage() * 100;
	const flexRemaining = (1 - getCurrentTimePercentage()) * 100;

	return (
		<View style={styles.container}>
			<TouchableOpacity style={styles.fullScreen} onPress={() => setState({ ...state, paused: !state.paused })}>
				<Video
					ref={(ref) => {
						video = ref;
					}}
					source={source}
					style={styles.fullScreen}
					rate={state.rate}
					paused={state.paused}
					volume={state.volume}
					muted={state.muted}
					resizeMode={state.resizeMode}
					onLoad={onLoad}
					onProgress={onProgress}
					onEnd={onEnd}
					onAudioBecomingNoisy={onAudioBecomingNoisy}
					onAudioFocusChanged={onAudioFocusChanged}
					repeat={false}
				/>
			</TouchableOpacity>

			<View style={styles.controls}>
				<View style={styles.generalControls}>
					<View style={styles.volumeControl}>
						<Text style={styles.textPlay}>{state.paused ? 'TAP TO PLAY' : 'TAP TO PAUSE'}</Text>
					</View>
				</View>
				<View style={styles.trackingControls}>
					<View style={styles.progress}>
						<View style={[ styles.innerProgressCompleted, { flex: flexCompleted } ]} />
						<View style={[ styles.innerProgressRemaining, { flex: flexRemaining } ]} />
					</View>
				</View>
			</View>
		</View>
	);
};

export default VideoView;

const styles = ScaledSheet.create({
	container              : {
		flex            : 1,
		justifyContent  : 'center',
		alignItems      : 'center',
		backgroundColor : 'black'
	},
	fullScreen             : {
		position : 'absolute',
		top      : 0,
		left     : 0,
		bottom   : 0,
		right    : 0
	},
	controls               : {
		backgroundColor : 'transparent',
		borderRadius    : 5,
		position        : 'absolute',
		bottom          : 20,
		left            : 20,
		right           : 20
	},
	progress               : {
		flex          : 1,
		flexDirection : 'row',
		borderRadius  : 3,
		overflow      : 'hidden'
	},
	innerProgressCompleted : {
		height          : 20,
		backgroundColor : '#cccccc'
	},
	innerProgressRemaining : {
		height          : 20,
		backgroundColor : '#2C2C2C'
	},
	generalControls        : {
		flex          : 1,
		flexDirection : 'row',
		borderRadius  : 4,
		overflow      : 'hidden',
		paddingBottom : 10
	},
	rateControl            : {
		flex           : 1,
		flexDirection  : 'row',
		justifyContent : 'center'
	},
	volumeControl          : {
		flex           : 1,
		flexDirection  : 'row',
		justifyContent : 'center'
	},
	textPlay               : {
		fontFamily : 'GothamBold',
		color      : '#fff'
	},
	resizeModeControl      : {
		flex           : 1,
		flexDirection  : 'row',
		alignItems     : 'center',
		justifyContent : 'center'
	},
	controlOption          : {
		alignSelf    : 'center',
		fontSize     : 11,
		color        : 'white',
		paddingLeft  : 2,
		paddingRight : 2,
		lineHeight   : 12
	}
});
