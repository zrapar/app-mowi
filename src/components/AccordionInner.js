import React, { useEffect, useState } from 'react';
import { ScaledSheet } from 'react-native-size-matters';
import {
	Text,
	Dimensions,
	View,
	FlatList,
	BackHandler,
	TouchableOpacity,
	ScrollView,
	ActivityIndicator,
	Image
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import imageIcon from '../assets/images/cloud-computing.png';
import documentIcon from '../assets/images/document.png';
import playIcon from '../assets/images/play.png';
import { convertUrlToCacheFile } from '../utils/utils';
import RNFetchBlob from 'rn-fetch-blob';
import { Overlay } from 'react-native-elements';
import { setFile } from '../redux/actions/brands';
import { Grid, Row, Col } from 'react-native-easy-grid';
const { height, width } = Dimensions.get('window');

const AccordionInner = ({ feature, type, navigation }) => {
	const dispatch = useDispatch();
	const Brand = useSelector(({ brands }) => brands.selectedBrand);
	const isConnected = useSelector(({ auth }) => auth.connectionStatus);
	const isProduct = feature === 'products';

	const [ isVisible, toggleVisible ] = useState(false);
	const [ warningConex, toggleWarning ] = useState(false);
	const [ progress, setProgress ] = useState(0);
	// const [ categories, setCategories ] = useState([]);
	const [ selectedFeature, setSelected ] = useState([]);
	const [ loading, toggleLoading ] = useState(true);

	useEffect(
		() => {
			// let addCategories = [];
			let filterBrand = Brand[feature]
				.filter((item) => item.type === type)
				.map((item) => {
					let videos = item.videos
						? item.videos.map((o, index) => {
								return {
									url   : o,
									name  : `${item.name}-${index}`,
									uuid  : item.uuid,
									index,
									type  : 'video'
								};
							})
						: null;
					let documents = item.documents
						? item.documents.map((o, index) => {
								let elem = {
									url   : o,
									name  : `${item.name}-${index}`,
									uuid  : item.uuid,
									index,
									type  : 'document'
								};
								if (item.hasOwnProperty('categories')) {
									elem['categories'] = item.categories.map((cat) => {
										// addCategories.push({ name: cat.name, uuid: cat.uuid });
										return cat.name;
									});
								}
								return elem;
							})
						: null;

					if (feature === 'videos') {
						return videos;
					} else {
						return documents;
					}
				})
				.flat();
			setSelected(filterBrand);
			toggleLoading(false);
		},
		[ feature ]
	);

	// useEffect(
	// 	() => {
	// 		if (isProduct) {
	// 		}
	// 	},
	// 	[ isProduct ]
	// );

	const renderItem = ({ item, ...rest }) => {
		return (
			<View style={style.viewButton}>
				<TouchableOpacity
					onPress={() => {
						if (isConnected) {
							if (item.url.includes('file')) {
								if (item.type === 'document') {
									navigation.navigate({
										routeName : 'PDFView',
										params    : { file: item.url, view: type }
									});
								} else {
									navigation.navigate({
										routeName : 'VideoView',
										params    : { file: item.url, view: type }
									});
								}
							} else {
								toggleVisible(true);
								RNFetchBlob.config({
									fileCache : true,
									session   : 'documents'
								})
									.fetch('GET', item.url)
									.progress((received, total) => {
										setProgress(parseInt(received / total * 100));
									})
									.then((response) => {
										const pathResolved = 'file://' + response.path();
										dispatch(
											setFile(feature, item.uuid, pathResolved, Brand.uuid, item.index, item.type)
										);
										setProgress(100);
										setTimeout(() => {
											toggleVisible(false);
										}, 10);
									});
							}
						} else {
							toggleWarning(true);
						}
					}}
					style={style.containerBtn}
				>
					<Grid>
						<Row style={{ justifyContent: 'center', alignContent: 'space-around', alignItems: 'center' }}>
							<Image
								source={
									item.url.includes('file') ? item.type === 'document' ? (
										documentIcon
									) : (
										playIcon
									) : (
										imageIcon
									)
								}
								style={{ width: 50, height: 50 }}
							/>

							{isProduct ? (
								<View>
									<Text style={style.title}>{item.name}</Text>
									<Text style={style.title}>Categories: {item.categories.join(', ')}</Text>
								</View>
							) : (
								<Text style={style.title}>{item.name}</Text>
							)}
						</Row>
					</Grid>
				</TouchableOpacity>
			</View>
		);
	};

	const emptyData = ({ item, ...rest }) => {
		return (
			<View style={style.viewButton}>
				<TouchableOpacity
					onPress={() => {
						console.log('holis');
					}}
					style={style.containerBtn}
				>
					<Grid>
						<Row style={{ justifyContent: 'center', alignContent: 'space-around', alignItems: 'center' }}>
							<Text style={style.title}>Nothing to display</Text>
						</Row>
					</Grid>
				</TouchableOpacity>
			</View>
		);
	};

	const capitalize = (string) => {
		return string.charAt(0).toUpperCase() + string.slice(1);
	};

	return (
		<React.Fragment>
			{loading ? (
				<ActivityIndicator />
			) : (
				<FlatList
					contentContainerStyle={style.containerList}
					scrollEventThrottle={1000}
					data={selectedFeature}
					renderItem={renderItem}
					keyExtractor={(item) => item.name}
					ListEmptyComponent={emptyData}
				/>
			)}

			<Overlay
				isVisible={isVisible}
				windowBackgroundColor='rgba(255, 255, 255, .5)'
				overlayBackgroundColor='#fff'
				width='auto'
				height='auto'
			>
				<Text>{`Descargando: ${progress}%`}</Text>
			</Overlay>
			<Overlay
				isVisible={warningConex}
				windowBackgroundColor='rgba(255, 255, 255, .5)'
				overlayBackgroundColor='#fff'
				onBackdropPress={() => toggleWarning(false)}
				width='auto'
				height='auto'
			>
				<Text>You cannot download this content, because you are not connected to any network</Text>
			</Overlay>
		</React.Fragment>
	);
};

const style = ScaledSheet.create({
	viewContainer : {
		width  : `${width}@s`,
		height : `${height}@vs`,
		margin : '0@vs'
	},
	containerList : {
		justifyContent : 'space-around',
		flexDirection  : 'row',
		display        : 'flex'
	},
	viewButton    : {
		margin         : '5@s',
		justifyContent : 'center',
		alignItems     : 'center'
	},
	containerBtn  : {
		justifyContent    : 'center',
		alignItems        : 'center',
		backgroundColor   : '#fff',
		width             : '95%',
		height            : '70@vs',
		borderWidth       : 1,
		borderRadius      : 15,
		borderColor       : 'transparent',
		shadowColor       : '#fa4169',
		shadowOffset      : { width: 0, height: 2 },
		shadowOpacity     : 0.5,
		shadowRadius      : 1,
		elevation         : 8,
		marginLeft        : '10@vs',
		marginRight       : '10@vs',
		paddingHorizontal : '20@vs'
	},
	title         : {
		color      : '#73628B',
		textAlign  : 'left',
		fontSize   : '12@vs',
		fontFamily : 'GothamMedium',
		marginLeft : '10@s'
	}
});

export default AccordionInner;
