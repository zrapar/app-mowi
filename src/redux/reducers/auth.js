import { AuthTypes } from '../types';
import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer } from 'redux-persist';

const initialState = {
	user             : null,
	isAuth           : false,
	loadingAuth      : false,
	token            : '',
	error            : false,
	connectionStatus : false
};

const reducer = (state = initialState, action) => {
	const { type, payload } = action;
	switch (type) {
		case AuthTypes.START_RQUEST: {
			return {
				...state,
				loadingAuth : true
			};
		}
		case AuthTypes.LOGIN_SUCCESS: {
			return {
				...state,
				user        : payload.user,
				isAuth      : true,
				loadingAuth : false,
				token       : payload.token,
				error       : false
			};
		}
		case AuthTypes.LOGIN_FAIL: {
			return {
				...state,
				loadingAuth : false,
				error       : true
			};
		}
		case AuthTypes.LOGOUT: {
			return {
				...state,
				...initialState
			};
		}
		case AuthTypes.SET_STATUS_CONNECTION: {
			return {
				...state,
				connectionStatus : payload
			};
		}
		default: {
			return state;
		}
	}
};

const persistConfig = {
	key     : 'auth',
	storage : AsyncStorage
};

const authReducer = persistReducer(persistConfig, reducer);

export default authReducer;
