import { BrandsTypes } from '../types';
import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer } from 'redux-persist';

const initialState = {
	brands        : [],
	loadingBrands : false,
	foundBrands   : false,
	selectedBrand : null,
	getBrand      : false
};

const reducer = (state = initialState, action) => {
	const { type, payload } = action;
	switch (type) {
		case BrandsTypes.START_RQUEST: {
			return {
				...state,
				loadingBrands : true,
				brands        : []
			};
		}
		case BrandsTypes.GET_BRANDS: {
			let exists = state.brands.find((o) => o.uuid === payload.uuid);
			return {
				...state,
				brands        : exists ? [ ...state.brands ] : [ ...state.brands, payload ],
				// brands        : [ payload ],
				error         : false,
				foundBrands   : true,
				selectedBrand : null,
				getBrand      : false
			};
		}
		case BrandsTypes.SET_BRANDS_URL: {
			return {
				...state,
				brands : payload
			};
		}
		case BrandsTypes.SET_FILE_FEATURE: {
			let newSelected = null;
			const newBrands = state.brands.map((item) => {
				if (item.uuid === payload.brandUuid) {
					const changeFeature = item[payload.feature].map((i) => {
						if (i.uuid === payload.uuid) {
							let edited = i;
							if (item.type === 'document') {
								edited.documents[payload.index] = payload.file;
							} else {
								edited.videos[payload.index] = payload.file;
							}
							return edited;
						} else {
							return i;
						}
					});
					let editedItem = item;
					editedItem[payload.feature] = changeFeature;
					newSelected = editedItem;
					return editedItem;
				} else {
					return item;
				}
			});

			return {
				...state,
				brands        : newBrands,
				selectedBrand : newSelected
			};
		}
		case BrandsTypes.SET_BRAND: {
			return {
				...state,
				selectedBrand : null
			};
		}
		case BrandsTypes.LOADING_BRANDS: {
			return {
				...state,
				loadingBrands : false
			};
		}
		case BrandsTypes.GET_BRAND: {
			return {
				...state,
				selectedBrand : payload,
				getBrand      : true
			};
		}
		case BrandsTypes.ERROR_BRANDS: {
			return {
				...state,
				loadingBrands : false,
				error         : true,
				foundBrands   : false
			};
		}

		default: {
			return state;
		}
	}
};

const persistConfig = {
	key     : 'brands',
	storage : AsyncStorage
};

const brandsReducer = persistReducer(persistConfig, reducer);

export default brandsReducer;
