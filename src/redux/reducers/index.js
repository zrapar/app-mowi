import { combineReducers } from 'redux';
import authReducer from './auth';
import brandsReducer from './brands';
import { createNavigationReducer } from 'react-navigation-redux-helpers';
import appNavigator from '../../routes';

export const rootReducer = () =>
	combineReducers({
		auth   : authReducer,
		nav    : createNavigationReducer(appNavigator),
		brands : brandsReducer
	});
