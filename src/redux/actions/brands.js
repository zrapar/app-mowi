import axiosInstance from '../../utils/axiosInstace';
import { BrandsTypes, AuthTypes } from '../types';
import { convertToPromise, resolvePromises, convertUrlToCacheFile } from '../../utils/utils';
import RNFetchBlob from 'rn-fetch-blob';
import { setAuth, checkUser } from './auth';

export const getBrands = (navigation) => async (dispatch, getState) => {
	if (!axiosInstance.defaults.headers.common.hasOwnProperty('Authorization')) {
		setAuth(getState().auth.token);
	}
	try {
		const { data } = await axiosInstance.get('/brands');
		const parseData = JSON.parse(data);

		if (parseData.success) {
			dispatch({ type: BrandsTypes.START_RQUEST });
			// dispatch({
			// 	type    : BrandsTypes.SET_BRANDS_URL,
			// 	payload : parseData.data
			// });
			// const promises = convertToPromise(parseData.data);
			// const allData = await resolvePromises(promises);
			// console.log(allData);
			let acum = 0;
			parseData.data
				.map((element) => {
					return {
						...element,
						image      : convertUrlToCacheFile(element.image, 'brands'),
						background : convertUrlToCacheFile(element.background, 'brands')
					};
				})
				.forEach((element) => {
					if (element instanceof Promise) {
						element.then((item) => {
							item.image.then((res) => {
								item.background.then((res2) => {
									acum += 1;
									dispatch({
										type    : BrandsTypes.GET_BRANDS,
										payload : {
											...item,
											image      : res,
											background : res2
										}
									});
									if (parseData.data.length === acum) {
										dispatch({ type: BrandsTypes.LOADING_BRANDS, payload: res });
									}
								});
							});
						});
					} else {
						element.image.then((res) => {
							element.background.then((res2) => {
								acum += 1;
								dispatch({
									type    : BrandsTypes.GET_BRANDS,
									payload : {
										...element,
										image      : res,
										background : res2
									}
								});
								if (parseData.data.length === acum) {
									dispatch({ type: BrandsTypes.LOADING_BRANDS, payload: res });
								}
							});
						});
					}
				});
			navigation.navigate('Home');
		}
	} catch (err) {
		console.log(err);
		console.log(err.status);
		if (err.response.status === 401) {
			dispatch({ type: AuthTypes.LOGOUT });
		}
		dispatch({ type: BrandsTypes.ERROR_BRANDS });
		console.log(err.response);
		console.log(err.request);
	}
};

export const getBrand = (uuid) => async (dispatch, getState) => {
	try {
		const brand = getState().brands.brands.find((o) => o.uuid === uuid);

		return dispatch({ type: BrandsTypes.GET_BRAND, payload: brand });
	} catch (err) {
		console.log(err);
		console.log(err.status);
		dispatch({ type: BrandsTypes.ERROR_BRANDS });
		console.log(err.response);
		console.log(err.request);
	}
};

export const cleanSelectedBrand = () => async (dispatch) => {
	return dispatch({ type: BrandsTypes.SET_BRAND });
};

export const setFile = (feature, uuid, file, brandUuid, index, type) => async (dispatch) => {
	return dispatch({ type: BrandsTypes.SET_FILE_FEATURE, payload: { feature, uuid, file, brandUuid, index, type } });
};
