import axiosInstance from '../../utils/axiosInstace';
import { AuthTypes } from '../types';

export const login = (navigation) => async (dispatch, getState) => {
	try {
		const { data } = await axiosInstance.post('/auth/login', { email: 'admin@admin.com', password: '123456789' });
		if (data.success) {
			setAuth(data.token);
			return dispatch({ type: AuthTypes.LOGIN_SUCCESS, payload: data });
		}
	} catch (err) {
		console.log(err.status);
		dispatch({ type: AuthTypes.LOGOUT });
		console.log(err.response);
		console.log(err.request);
	}
};

export const connectionStatus = (status) => async (dispatch) => {
	dispatch({ type: AuthTypes.SET_STATUS_CONNECTION, payload: status });
};

export const checkUser = () => async (dispatch) => {
	try {
		console.log('asd');
		// setAuth(token);
		const { data } = await axiosInstance.get('/auth/me');
		if (data.data) {
			// TODO: MAKE A WELCOME BACK
			// navigation.navigate('Home');
		}
	} catch (err) {
		console.log(err);
		dispatch({ type: AuthTypes.LOGOUT });
		console.log(err.response);
		console.log(err.request);
	}
};

export const setAuth = (accessToken) => {
	axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
};
