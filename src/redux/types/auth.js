//AUTH

export const AuthTypes = {
	START_RQUEST          : '@@Auth/START RQUEST',
	LOGIN_SUCCESS         : '@@Auth/LOGIN SUCCESS',
	LOGIN_FAIL            : '@@Auth/LOGIN FAIL',
	LOGOUT                : '@@Auth/LOGOUT',
	SET_STATUS_CONNECTION : '@@Auth/SET STATUS CONNECTION'
};
