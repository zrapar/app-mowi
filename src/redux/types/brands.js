//AUTH

export const BrandsTypes = {
	START_RQUEST     : '@@Brands/START RQUEST',
	GET_BRANDS       : '@@Brands/GET BRANDS',
	GET_BRAND        : '@@Brands/GET BRAND',
	ERROR_BRANDS     : '@@Brands/ERROR GETTING BRANDS',
	LOADING_BRANDS   : '@@Brands/LOADING BRANDS',
	SET_BRANDS_URL   : '@@Brands/SET BRANDS URL',
	SET_BRAND        : '@@Brands/SET BRAND',
	SET_FILE_FEATURE : '@@Brands/SET FILE FEATURE'
};
