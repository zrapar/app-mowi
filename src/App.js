import React, { useState, useEffect } from 'react';
import { BackHandler, StatusBar } from 'react-native';
import { Provider, connect } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Orientation from 'react-native-orientation-locker';
import { configureStore } from './store/configureStore';
import { createReduxContainer } from 'react-navigation-redux-helpers';
import AppNavigator from './routes';

const appContainer = createReduxContainer(AppNavigator);

const mapStateToProps = (state) => ({
	state : state.nav
});

const AppWithNavigationState = connect(mapStateToProps)(appContainer);

const App = () => {
	const initialOrientation = Orientation.getInitialOrientation();
	const { store, persistor } = configureStore();

	useEffect(() => {
		if (initialOrientation === 'PORTRAIT') {
			Orientation.lockToLandscape();
		}
	}, []);

	return (
		<Provider store={store}>
			{/* <StatusBar backgroundColor='#f3704e' /> */}
			<PersistGate loading={null} persistor={persistor}>
				<AppWithNavigationState />
			</PersistGate>
		</Provider>
	);
};

export default App;
