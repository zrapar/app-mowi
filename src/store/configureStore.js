import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';
import { persistStore } from 'redux-persist';
import { rootReducer } from '../redux/reducers';
import Config from 'react-native-config';

export const configureStore = () => {
	// create the composing function for our middlewares
	const composeEnhancers = composeWithDevTools({});

	// create navigation middleware
	const navMiddleware = createReactNavigationReduxMiddleware((state) => state.nav);

	const client = axios.create({
		baseURL      : Config.API_URL,
		responseType : 'json'
	});

	const store = createStore(
		rootReducer(),
		composeEnhancers(applyMiddleware(navMiddleware, axiosMiddleware(client), thunk))
	);

	const persistor = persistStore(store, {
		key : 'mowi'
	});

	return {
		store,
		persistor
	};
};
