import Home from '../containers/Home';
import SelectBrand from '../containers/SelectBrand';
import SelectType from '../containers/SelectType';
import FeaturesByType from '../containers/FeaturesByType';
import PDFView from '../components/PDFView';
import VideoView from '../components/VideoView';

const routes = {
	Home        : {
		screen : Home
	},
	SelectBrand : {
		screen : SelectBrand
	},
	SelectType  : {
		screen : SelectType
	},
	Features    : {
		screen : FeaturesByType
	},
	PDFView     : {
		screen : PDFView
	},
	VideoView   : {
		screen : VideoView
	}
};

export default routes;
