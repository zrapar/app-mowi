import AuthLoading from '../containers/AuthLoading';
import NoConnection from '../containers/NoConnection';

const routes = {
	AuthLoading  : {
		screen : AuthLoading
	},
	NoConnection : {
		screen : NoConnection
	}
};

export default routes;
