import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AuthRoutes from './auth';
import HomeRoutes from './home';

const AppNavigator = createStackNavigator(
	{
		...AuthRoutes,
		...HomeRoutes
	},
	{
		headerMode       : 'none',
		initialRouteName : 'AuthLoading'
	}
);

export default createAppContainer(AppNavigator);
